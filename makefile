all: run clean
compile:
	gcc -g -O0 -Wall -Wextra -Werror -std=c99 -o 201406463 src/201406463.c
run: compile
	touch MP.in
	valgrind --leak-check=full --main-stacksize=8192 ./201406463
clean:
	rm -f 201406463* *.in *.out