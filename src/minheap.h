#ifndef MINHEAP_H
#define MINHEAP_H

#include "utils.h"

typedef struct KeyPriority
{
	ll key;
	ll priority;
} KeyPriority;


typedef struct MinHeap
{
	KeyPriority *heap;
	ll size;
	ll maxsize;
	ll maxnodes;
	ll *dat;
} MinHeap;

void swap_keypriority(MinHeap *mh, ll a, ll b)
{
	ll temp = mh->heap[a].key;
	mh->heap[a].key = mh->heap[b].key;
	mh->heap[b].key = temp;

	temp = mh->heap[a].priority;
	mh->heap[a].priority = mh->heap[b].priority;
	mh->heap[b].priority = temp;

	mh->dat[mh->heap[a].key] = a;
	mh->dat[mh->heap[b].key] = b;
}

MinHeap *minheap(ll maxsize, ll maxnodes)
{
	MinHeap *mh = NEW(MinHeap);
	mh->heap = NEWARR(KeyPriority, maxsize);
	mh->size = 0;
	mh->maxsize = maxsize;
	mh->maxnodes = maxnodes;
	mh->dat = NEWARR(ll, maxnodes);
	REP (i, maxnodes)
	{
		mh->dat[i] = -1;
	}
	return mh;
}

void _minheap(MinHeap *mh)
{
	delete(mh->heap);
	delete(mh->dat);
	delete(mh);
}

void siftup(MinHeap *mh, ll i)
{
	while (i > 0)
	{
		ll father = (i-1)/2;
		if (mh->heap[i].priority < mh->heap[father].priority)
		{
			swap_keypriority(mh, i, father);
			i = father;
		}
		else
		{
			break;
		}
	}
}

void siftdown(MinHeap *mh, ll i)
{
	while (true)
	{
		ll son1 = i*2 + 1;
		ll son2 = son1 + 1;
		ll lesser_son;
		if (i >= mh->size || son1 >= mh->size) break;

		if (son2 < mh->size)
		{
			lesser_son = mh->heap[son1].priority < mh->heap[son2].priority ? son1 : son2;
		}
		else
		{
			lesser_son = son1;
		}

		if (mh->heap[i].priority > mh->heap[lesser_son].priority)
		{
			swap_keypriority(mh, i, lesser_son);
			i = lesser_son;
		}
		else
		{
			break;
		}
	}
}

void add_with_priority(MinHeap *mh, ll key, ll priority)
{
	ll i = mh->size++;
	mh->heap[i].key = key;
	mh->heap[i].priority = priority;
	mh->dat[key] = i;
	siftup(mh, i);
}

ll extract_min(MinHeap *mh)
{
	ll i = --mh->size;
	swap_keypriority(mh, 0, i);
	siftdown(mh, 0);
	mh->dat[mh->heap[i].key] = -1;
	return mh->heap[i].key;
}

void decrease_priority(MinHeap *mh, ll key, ll priority)
{
	mh->heap[mh->dat[key]].priority = priority;
	siftup(mh, mh->dat[key]);
}

void print_heap(MinHeap *mh)
{
	printf("\nheap\n");
	REP(i, mh->size)
	{
		printf("%lld: %lld => %lld\n", i, mh->heap[i].key, mh->heap[i].priority);
	}
	printf("dat\n");
	REP(i, mh->maxnodes)
	{
		printf("%lld => %lld\n", i, mh->dat[i]);
	}
}

#endif
