#ifndef UTILS_H
#define UTILS_H

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef long long ll;

#ifndef SCAN
#define SCAN(a,b,c) fscanf(in, "%lld%lld%lld", &a, &b, &c)
#endif

#ifndef REP
#define REP(i, size) for (ll i = 0; i < size; ++i)
#endif

#ifndef NEW
#define NEW(type) (type*)calloc(1, sizeof(type))
#endif

#ifndef NEWARR
#define NEWARR(type, size) (type*)calloc(size, sizeof(type))
#endif

#ifndef MAX
#define MAX(a, b) (a >= b ? a : b)
#endif

void delete(void *p)
{
	free(p);
	p = 0;
}

#endif
